This repository only has the Simplified-Chinese version. For the English version, please visit [GitHub](https://github.com/ltndkl/Counter-Strike-Augmented-Edition).

**由于 Gitee 的自身限制，此仓库不再发布 CSAE 发行版本。请前往 [GitHub](https://github.com/ltndkl/Counter-Strike-Augmented-Edition) 查看。**

# _Counter-Strike Augmented Edition_ 公开仓库
-   本仓库目前不包含任何 _CSAE_ 源码。

-   下载 _CSAE_ 代表你同意以下内容：
    1. _CSAE_ 本质上是用于 _CS1.6 build 3266_ 的 mod，与私服无关。
    2. 禁止将此 mod 和/或其内容用于任何商业目的，mod 制作组不承担与此相关的法律责任。
    3. 此 mod 仅供学习交流之用，请于下载后 24 小时内删除。

-   _CSAE_ 使用过以下全名：

    ~~_Counter-Strike Arcment Edition_~~

    ~~_Counter-Strike Avanzate Edition_~~

    现全名为 **_Counter-Strike Augmented Edition_**


[百度贴吧](https://tieba.baidu.com/csae)

[电报群](https://t.me/joinchat/JNYAMw3FyuWw81pHaG07JA)

请前往 [Releases](https://gitee.com/ltndkl/Counter-Strike-Augmented-Edition/releases) 下载 _CSAE_ 。

请前往 [Issues](https://gitee.com/ltndkl/Counter-Strike-Augmented-Edition/issues) 报告 BUG 或请求新功能。

